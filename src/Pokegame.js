import React , { Component } from 'react';
import Pokedex from './Pokedex';

class Pokegame extends Component {

    static defaultProps = {
        pokecards : [
            {id: 4, name: 'Charmander', type: 'fire', base_experience: 62},
            {id: 7, name: 'Squirtle', type: 'water', base_experience: 63},
            {id: 11, name: 'Metapod', type: 'bug', base_experience: 72},
            {id: 12, name: 'Butterfree', type: 'flying', base_experience: 178},
            {id: 25, name: 'Pikachu', type: 'electric', base_experience: 112},
            {id: 39, name: 'Jigglypuff', type: 'normal', base_experience: 95},
            {id: 94, name: 'Gengar', type: 'poison', base_experience: 225},
            {id: 133, name: 'Eevee', type: 'normal', base_experience: 65}
        ]
    };

    render() {

        let hand1 = [];
        let hand2 = [...this.props.pokecards];

        while(hand2.length > hand1.length){
            let randIdx = Math.floor(Math.random() * hand2.length);
            let randPoke = hand2.splice(randIdx, 1)[0];
            console.log(hand2);

            hand1.push(randPoke);
        }

        /*
        const calculatePoints = (hand) => {
            let total = 0;
            for(let i = 0; i < hand.length; i++){
                total = total + hand[i].base_experience;
            }
            return total;
        } 
        */

       const calculatePoints = (hand) => {
           console.log(hand);
           return(
               hand.reduce((exp, pokemon) => exp + pokemon.base_experience, 0)
           )
       };


        const totalHand1 = calculatePoints(hand1);
        const totalHand2 = calculatePoints(hand2);

        console.log(totalHand1);
        console.log(totalHand2);


        
        return(
            <div>
                <Pokedex hand={hand1} winner={(totalHand1 > totalHand2)} exp={totalHand1} />
                <Pokedex hand={hand2} winner={(totalHand2 > totalHand1)} exp={totalHand2} />
            </div>
        );
    }
}

export default Pokegame;