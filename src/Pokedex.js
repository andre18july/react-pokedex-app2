import React, { Component } from 'react';
import Pokecard from './Pokecard';
import './Pokedex.css';

class Pokedex extends Component {

    static defaultProps = {
        hand : [
            {id: 4, name: 'Charmander', type: 'fire', base_experience: 62},
            {id: 7, name: 'Squirtle', type: 'water', base_experience: 63},
            {id: 11, name: 'Metapod', type: 'bug', base_experience: 72},
            {id: 12, name: 'Butterfree', type: 'flying', base_experience: 178},
            {id: 25, name: 'Pikachu', type: 'electric', base_experience: 112},
            {id: 39, name: 'Jigglypuff', type: 'normal', base_experience: 95},
            {id: 94, name: 'Gengar', type: 'poison', base_experience: 225},
            {id: 133, name: 'Eevee', type: 'normal', base_experience: 65}
          ]
    };


    render(){

        let title;
        if(this.props.winner){
            title = <h2 className="Pokedex-Winner">Winner</h2>
        }else{
            title = <h2 className="Pokedex-Loser">Loser</h2>
        }

        return(
            <div className="Pokedex">
                {title}
                <h3>{this.props.exp}</h3>
                <div className="Pokedex-Cards">
                    {this.props.hand.map((el) => {
                        return(
                            <Pokecard
                                key={el.id} 
                                id={el.id}
                                name={el.name} 
                                type={el.type}
                                exp={el.base_experience}
                            />
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default Pokedex;

