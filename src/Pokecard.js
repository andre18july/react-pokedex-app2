import React , { Component } from 'react';
import './Pokecard.css';

class Pokecard extends Component {

    render() {

        /*
        const updateId = () => {
            let idToString = this.props.id.toString();
            if(idToString.length === 2){
                idToString = "0" + idToString;
            }else if(idToString.length === 1){
                idToString = "00" + idToString;
            }
            return idToString;
        }
        */

        const updateId = (number) => {
            return number<=999 ? `00${number}`.slice(-3) : number; 
        }

         
        const id = updateId(this.props.id);

        const imgUrl = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${id}.png`


        return(
            <div className="Pokecard">
                <h2 className="Pokecard-Title">{this.props.name}</h2>
                <div className="Pakecard-Img">
                    <img alt={this.props.name} src={imgUrl}/>
                </div>
                <div className="Pokecard-Data">Type: {this.props.type}</div>
                <div className="Pokecard-Data">EXP: {this.props.exp}</div>
            </div>
        ) 
    }
}

export default Pokecard;